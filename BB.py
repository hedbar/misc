import random

import math


class Feeder:
    def __init__(self):
        self.tape = {0:0}
        self.head = 0  # position of the tape head
        self.min_pos = 0
        self.max_pos = 0

    def read(self):
        """Read the symbol at the current tape head position."""

        if self.head not in self.tape:
            self.tape[self.head] = 0

        return self.tape[self.head]

    def write(self, symbol):
        """Write a symbol at the current tape head position."""
        self.tape[self.head] = symbol

    def move(self, direction):
        """Move the tape head in the given direction."""
        if direction == 'R':
            self.head += 1
            self.max_pos = max(self.max_pos,self.head)
        elif direction == 'L':
            self.head -= 1
            self.min_pos = min(self.min_pos,self.head)

    def __str__(self):
        return f'Tape: pos: {self.head}, val: {self.read()}'


class TuringMachine:
    def __init__(self, transitions,state,halt_):
        self.transitions = transitions
        self.state = state
        self.prev_state = None
        self.halt_ = halt_

    def read(self, symbol):
        next_state, write, move = self.transitions[(self.state, symbol)]
        self.prev_state = self.state
        self.state = next_state
        return (write, move) if next_state != self.halt_ else (None, None)


    @classmethod
    def random_machine(cls):
        """Randomly generate a Turing machine."""
        states = ['A', 'B', 'C', 'H']  # including halt state 'H'
        symbols = [0, 1]
        directions = ['L', 'R']

        transitions = {}
        for state in states:
            for symbol in symbols:
                next_state = random.choice(states)
                write = random.choice(symbols)
                move = random.choice(directions)
                transitions[(state, symbol)] = (next_state, write, move)
        return cls(transitions,states[0],states[-1])



def print_state(feeder, TM):
    feeder.read()
    # Convert the tape dict to a sorted list of values
    tape_list = [value for key, value in sorted(feeder.tape.items())]

    tape_zero_position = abs(min(feeder.tape.keys()))

    # Construct the tape string with '|' separator
    tape_str = ''.join(map(str, tape_list))
    tape_str = tape_str[:tape_zero_position] + '|' + tape_str[tape_zero_position:]

    # Construct the head position string with '*' indicator
    head_str = ' ' * ((feeder.head-feeder.min_pos) + (1 if feeder.head>=0 else 0) ) + '*'  # +1 to account for '|'

    # Get the current transition
    current_value = feeder.read()
    transition = str((TM.state, current_value)) + ' => '+ str(TM.transitions.get((TM.state, current_value), None))

    # Print the notation
    my_log(head_str)
    my_log(tape_str)
    my_log(f"Transition: {transition}")



interesting_transitions = \
    {('A', 0): ('B', 1, 'R'), ('A', 1): ('H', 1, 'L'), ('B', 0): ('C', 1, 'L'), ('B', 1): ('C', 1, 'R'), ('C', 0): ('B', 0, 'R'), ('C', 1): ('B', 1, 'L'), ('H', 0): ('C', 1, 'L'), ('H', 1): ('B', 1, 'L')}

def run_experiment(tm = None):
    tm = tm or TuringMachine.random_machine()
    feeder = Feeder()

    my_log(  '\n'.join(f" {key}: {value}" for key, value in tm.transitions.items()) )
    my_log('\n\n')

    MAX_STEPS = 100
    past_edge_moves_left = set()
    past_edge_moves_right = set()
    is_halt = False

    for i in range(MAX_STEPS+1):

        # my_log(f'Step:{i} ({feeder.head+1}, {symbol}, {tm.prev_state}) => ({move}, {tm.state})')
        my_log(f'\nStep:{i}')
        print_state(feeder, tm)
        symbol = feeder.read()

        if feeder.head == feeder.min_pos:
            if (tm.state,symbol) in past_edge_moves_left:
                break
            else:
                past_edge_moves_left.add((tm.state,symbol))
        if feeder.head == feeder.max_pos:
            if (tm.state,symbol) in past_edge_moves_right:
                break
            else:
                past_edge_moves_right.add((tm.state,symbol))


        write, move = tm.read(symbol)

        if write is None:
            is_halt = True
            break
        feeder.write(write)
        feeder.move(move)

    is_halt |= (i == MAX_STEPS)
    return (is_halt,i, tm)


def my_log(s):
    print(s)
    pass

tm = TuringMachine.random_machine()
tm.transitions = interesting_transitions
run_experiment(tm)
exit(0)



interesting_tm = []
N = 10000
halted_exp = 0
for i in range(N):
    is_halt, step_no, tm = run_experiment()
    if step_no > 10:
        interesting_tm.append( (is_halt,step_no,tm) )
    halted_exp += is_halt
    print(f'{is_halt}\t{step_no + 1}')

print(f'\t\tExperiment halted in {halted_exp} out of {N} experiments.')

for is_halt,step_no,tm in interesting_tm:
    print(f'\t\t{is_halt}\t{step_no + 1}\t{tm.transitions}')
